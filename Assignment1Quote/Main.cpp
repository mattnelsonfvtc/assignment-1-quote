// Matt Nelson
// Assignment 1

#include <iostream>
#include <conio.h>

int main()
{
	int number;

	std::cout << "Please input an integer between 1 and 5.";
	std::cin >> number;

	if (1 > number > 5) 
	{
		std::cout << "Sorry, this is not a valid input.";
	}
	else
	{
		for (int count = 0; count <= number ; count++)
		{
			std::cout << "\"Those who dare to fail miserably can achieve greatly.\"    -John F. Kennedy\n";
		}
	}

	_getch();
}